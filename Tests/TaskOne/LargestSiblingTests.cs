using System;
using Xunit;
using TaskOne;

namespace Tests.TaskOne
{
    public class LargestSiblingTests
    {
        private Finder _finder;

        public LargestSiblingTests()
        {
            _finder = new Finder();
        }

        [Theory]
        [InlineData(231, 321)]
        [InlineData(123, 321)]
        [InlineData(321, 321)]
        [InlineData(335, 533)]
        [InlineData(899, 998)]
        public void FindLargestSiblingTest(int input, int expectedLargest)
        {
            var largestSibling = _finder.LargestSiblingOf(input);
            Assert.Equal(expectedLargest, largestSibling);
        }

        [Fact]
        public void WhenLargestSiblingIsGreaterThan100000000ReturnsNonOK()
        {
            int unsupportedValue = 100000002;
            var result = _finder.LargestSiblingOf(unsupportedValue);
            Assert.Equal(-1, result);
        }
    }
}
