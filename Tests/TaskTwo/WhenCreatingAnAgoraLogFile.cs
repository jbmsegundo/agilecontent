﻿using System;
using System.IO;
using System.Text;
using TaskTwo;
using Xunit;

namespace Tests.TaskTwo
{
    public class WhenCreatingAnAgoraLogFile : IDisposable
    {
        private readonly string fileName;
        private readonly AgoraLogFileHandler logFileHandler;

        public WhenCreatingAnAgoraLogFile() 
        {
            fileName = $"agora-test-file-creation-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            logFileHandler = new AgoraLogFileHandler(fileName);
        }

        [Fact]
        public void AHeaderIsDefined()
        {
            var expectedFileVersion = 1.0;
            var expectedColumns = "provider http-method status-code uri-path time-taken response-size cache-status";
   
            logFileHandler.CreateFile();

            var actualFileVersion = logFileHandler.FileVersion;
            var actualFileDateCreation = logFileHandler.FileDateCreation;

            Assert.Equal(expectedFileVersion, actualFileVersion);
            Assert.InRange(actualFileDateCreation, DateTime.Now.AddSeconds(-1), DateTime.Now);
            Assert.Equal(expectedColumns, logFileHandler.FileColumns);
        }

        public void Dispose()
        {
            File.Delete(fileName);
        }
    }

}