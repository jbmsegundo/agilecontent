using System;
using System.IO;
using TaskTwo;
using Xunit;

namespace Tests.TaskTwo
{
    public class WhenAddingNewLogEntryToAgoraLogFile : IDisposable
    {
        private readonly string fileName;
        private readonly AgoraLogFileHandler logFileHandler;

        public WhenAddingNewLogEntryToAgoraLogFile()
        {
            fileName = $"agora-test-file-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            logFileHandler = new AgoraLogFileHandler(fileName);
        }
        [Fact]
        public void ANewLineIsAddedToTheFile()
        {

            string provider = "MINHA CDN";
            string httpMethod = "GET";
            int statusCode = 200;
            string uriPath = "/robots.txt";
            int timeTaken = 400;
            int responseSize = 600;
            string cacheStatus = "HIT";
            logFileHandler.CreateFile();
            var agoraLogEntry = new AgoraLogEntry(provider, httpMethod, statusCode, uriPath, timeTaken, responseSize, cacheStatus);
            logFileHandler.AddNewEntry(agoraLogEntry);
            AgoraLogEntry lastLogEntry = logFileHandler.LastLogEntry;
            Assert.Equal(provider, lastLogEntry.Provider);
            Assert.Equal(httpMethod, lastLogEntry.HttpMethod);
            Assert.Equal(statusCode, lastLogEntry.StatusCode);
            Assert.Equal(uriPath, lastLogEntry.UriPath);
            Assert.Equal(timeTaken, lastLogEntry.TimeTaken);
            Assert.Equal(responseSize, lastLogEntry.ResponseSize);
            Assert.Equal(cacheStatus, lastLogEntry.CacheStatus);
           
        }
        public void Dispose()
        {
            File.Delete(fileName);
        }
    }
}