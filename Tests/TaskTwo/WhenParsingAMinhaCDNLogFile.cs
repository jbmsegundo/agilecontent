using System;
using System.IO;
using Xunit;
using TaskTwo;
namespace Tests.TaskTwo
{
    public class WhenParsingAMinhaCDNLogFile : IDisposable
    {
        private readonly string minhaCDNFilename;
        private readonly string agoraFileName;
        private LogService logService;
        private AgoraLogFileHandler agoraLogFileHandler;


        public WhenParsingAMinhaCDNLogFile()
        {
            agoraFileName = $"agora-test-file-from-minha-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            minhaCDNFilename = $"minhacdn-test-file-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            agoraLogFileHandler = new AgoraLogFileHandler(agoraFileName);
            logService = new LogService(agoraLogFileHandler);
            GivenAMinhaCDNFile();

        }
        [Fact]
        public void AAgoraFileIsCreated()
        {
           

            logService.CreateAgoraFromMinhaCDN(minhaCDNFilename);

            Assert.True(File.Exists(agoraFileName));
            
            var lastLogEntry = agoraLogFileHandler.LastLogEntry;
            Assert.Equal("MINHA CDN", lastLogEntry.Provider);
            Assert.Equal("GET", lastLogEntry.HttpMethod);
            Assert.Equal(200, lastLogEntry.StatusCode);
            Assert.Equal("/robots.txt", lastLogEntry.UriPath);
            Assert.Equal(100, lastLogEntry.TimeTaken);
            Assert.Equal(312, lastLogEntry.ResponseSize);
            Assert.Equal("HIT", lastLogEntry.CacheStatus);

        }

        public void Dispose()
        {
            File.Delete(minhaCDNFilename);
        }

        private void GivenAMinhaCDNFile()
        {
            using (FileStream fileStream = new FileStream(minhaCDNFilename, FileMode.CreateNew, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.WriteLine(@"312|200|HIT|""GET /robots.txt HTTP/1.1""|100.2");
                }
            }
        }
    }


}