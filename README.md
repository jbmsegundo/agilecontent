Few notes about this coding challenge.

It's been a little while the last time that I've written c# production code. Lately I've been working more with other language such as python and ruby.

I don't even have a windows machine at the moment, so I wrote this code on a linux enviroment, basically dotnet core 2.1 and vscode text editor, nothing else.

Yeah, for the TaskTwo there is a lot of integration tests and not that much unit test. Streams and FS are not easy things to mock and felt like for this challenge integration tests are enough. But I will leave another C# project that I've written back in 2016. It was also done with TDD and there is more unit tests, mock and acceptance tests. 

https://bitbucket.org/jbmsegundo/pay_gross/src/master/

Back to the Task Two or Second task(whatever you want to call)

To run on linux:

$ dotnet run --project Converter https://s3.amazonaws.com/uux-itaas-static/minha-cdn-logs/input-01.txt some-output-random-file.txt


To run all tests

$ dotnet run test Tests