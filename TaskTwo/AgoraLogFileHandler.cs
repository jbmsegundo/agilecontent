﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace TaskTwo
{
    public class AgoraLogFileHandler
    {
        private readonly string fileName;

        public AgoraLogFileHandler(string fileName)
        {
            this.fileName = fileName;
        }

        public void CreateFile()
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.WriteLine("#Version: 1.0");
                    streamWriter.WriteLine($"#Date: {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}");
                    streamWriter.WriteLine("#Fields: provider http-method status-code uri-path time-taken response-size cache-status");
                }
            }
        }

        public void AddNewEntry(AgoraLogEntry agoraLogEntry)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.WriteLine($@"""{agoraLogEntry.Provider}"" {agoraLogEntry.HttpMethod} {agoraLogEntry.StatusCode} {agoraLogEntry.UriPath} {agoraLogEntry.TimeTaken} {agoraLogEntry.ResponseSize} {agoraLogEntry.CacheStatus}");
                }
            }
        }

        public DateTime FileDateCreation
        {
            get
            {
                var rowContent = ReadLine(2);
                var creationDate = ExtractCreationDate(rowContent);
                return creationDate;
            }
        }

        public string FileColumns
        {
            get
            {
                var rowContent = ReadLine(3);
                string columns = ExtractColumns(rowContent);
                return columns;
            }
        }

        private string ExtractColumns(string rowContent)
        {
            string columns = rowContent.Replace("#Fields: ", string.Empty);
            return columns;
        }

        public double FileVersion
        {
            get
            {
                var rowContent = ReadLine(1);
                var version = ExtractVersionNumber(rowContent);
                return version;
            }

        }

        public AgoraLogEntry LastLogEntry
        {
            get
            {
                AgoraLogEntry agoraLogEntry = null;
                using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        while (!streamReader.EndOfStream)
                        {
                            string rowContent = streamReader.ReadLine();
                            if (streamReader.Peek() == -1)
                            {
                                agoraLogEntry = ExtractLogEntry(rowContent);
                            }
                        }
                    }
                }
                return agoraLogEntry;
            }
        }

        private AgoraLogEntry ExtractLogEntry(string rowContent)
        {
            var firstGroup = rowContent.Split('"');
            var secondGroup = firstGroup[2].Split(' ');
            var provider = firstGroup[1];
            
            string httpMethod = secondGroup[1];
            int statusCode = Convert.ToInt32(secondGroup[2]);
            string uriPath = secondGroup[3];
            int timeTaken = Convert.ToInt32(secondGroup[4]);
            int responseSize = Convert.ToInt32(secondGroup[5]);
            string cacheStatus = secondGroup[6];
            return new AgoraLogEntry(provider, httpMethod, statusCode, uriPath, timeTaken, responseSize, cacheStatus); 

        }

        private DateTime ExtractCreationDate(string rowContent)
        {
            var date = rowContent.Replace("#Date: ", string.Empty);
            return DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss",
            System.Globalization.CultureInfo.InvariantCulture);
        }

        private string ReadLine(int line)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader streamReader = new StreamReader(fileStream))
                {
                    string rowContent = "";
                    for (int i = 0; i < line; i++)
                    {
                        rowContent = streamReader.ReadLine();
                    }
                    return rowContent;
                }
            }
        }

        private double ExtractVersionNumber(string fileRowContent)
        {
            var version = fileRowContent.Replace("#Version: ", string.Empty);
            return Convert.ToDouble(version);
        }
    }
}