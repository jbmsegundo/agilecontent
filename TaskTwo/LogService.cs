using System;
using System.IO;

namespace TaskTwo
{
    public class LogService
    {
        private readonly AgoraLogFileHandler agoraLogFileHandler;

        public LogService(AgoraLogFileHandler agoraLogFileHandler)
        {
            this.agoraLogFileHandler = agoraLogFileHandler;
        }

        public void CreateAgoraFromMinhaCDN(string minhaCDNFilename)
        {
            agoraLogFileHandler.CreateFile();
            ParseMinhaCDNToAgoraContent(minhaCDNFilename);

        }

        private void ParseMinhaCDNToAgoraContent(string minhaCDNFilename)
        {
            using (FileStream fileStream = new FileStream(minhaCDNFilename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader streamReader = new StreamReader(fileStream))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string currentLine = streamReader.ReadLine();
                        AgoraLogEntry agoraLogEntry = CreateAgoraLogEntry("MINHA CDN", currentLine);
                        agoraLogFileHandler.AddNewEntry(agoraLogEntry);
                    }

                }
            }
        }

        private AgoraLogEntry CreateAgoraLogEntry(string sourceProvider, string currentLine)
        {
            var firstGroup = currentLine.Split('|');
            int responseSize = Convert.ToInt32(firstGroup[0]);
            int statusCode = Convert.ToInt32(firstGroup[1]);
            string cacheStatus = firstGroup[2];
            int timeTaken = (int)Convert.ToDouble(firstGroup[4]);

            var secondGroup = firstGroup[3].Split(' ');
            var httpMethod = secondGroup[0].Substring(1);
            var uriPath = secondGroup[1];
            var agoraLogEntry = new AgoraLogEntry(sourceProvider, httpMethod, statusCode, uriPath, timeTaken, responseSize, cacheStatus);
            return agoraLogEntry;

        }
    }
}