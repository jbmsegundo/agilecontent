﻿using System;
using System.Net;
using TaskTwo;

namespace Converter
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileURL = args[0];
            string minhaCDNFilename = $"minhacdn-file-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
            string targetPath = args[1];
            var webClient = new WebClient();
            webClient.DownloadFile(fileURL, minhaCDNFilename);
            AgoraLogFileHandler agoraLogFileHandler = new AgoraLogFileHandler(targetPath);
            LogService logService = new LogService(agoraLogFileHandler);
            logService.CreateAgoraFromMinhaCDN(minhaCDNFilename);
        }
    }
}
