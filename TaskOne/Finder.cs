﻿using System;
using System.Linq;

namespace TaskOne
{
    public class Finder
    {
        public int LargestSiblingOf(int input)
        {
            if(input > 100000000)
                return -1;
                
            var result = input.ToString()
            .Select(digit => Convert.ToInt32(digit.ToString()))
            .OrderByDescending(digit=> digit)
            .Select(digit => digit.ToString());
            var largestSibling = Convert.ToInt32(string.Join(String.Empty, result));
            return largestSibling;
        
        }
    }
}
